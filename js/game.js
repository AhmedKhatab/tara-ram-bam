(function(){
    // Functions
    function buildQuiz() {
      // variable to store the HTML output
      const output = [];
  
      // for each question...
      output.push(
        "<div class='slide'><img width='30%' src='../img/start.png'></div>"
      );
      myQuestions.forEach(
        (currentQuestion, questionNumber) => {
  
          // variable to store the list of possible answers
          const answers = [];
  
          // and for each available answer...
          for(letter in currentQuestion.answers){
  
            // ...add an HTML radio button
            answers.push(
              `<label class="answer-label">
                ${currentQuestion.answers[letter]}
                <input class="answer-radio" id="check-${questionNumber}" type="radio" name="question${questionNumber}" value="${letter}">
                <span class="checkmark"></span>
              </label>`
            );
          }
  
          // add this question and its answers to the output
          output.push(
            `<div class="slide">
              <div class="question"><img src="${currentQuestion.question}"></div>
              <div class="answers"> ${answers.join("")} </div>
            </div>`
          );
        }
      );

      output.push(
        "<div class='slide'><img width='30%' src='../img/thanks.png'></div>"
      );
  
      // finally combine our output list into one string of HTML and put it on the page
      quizContainer.innerHTML = output.join('');
    }
  
    function showResults() {  
      // gather answer containers from our quiz
      const answerContainers = quizContainer.querySelectorAll('.answers');
  
      // keep track of user's answers
  
      // for each question...
      myQuestions.forEach( (currentQuestion, questionNumber) => {
        
        // find selected answer
        if  (questionNumber === currentSlide - 2) {
          const answerContainer = answerContainers[questionNumber];
          const selector = `input[name=question${questionNumber}]:checked`;
          const userAnswer = (answerContainer.querySelector(selector) || {}).value;
          const bottom = Number(balon.style.bottom.split('p')[0])
          
          // if answer is correct
          if (userAnswer === currentQuestion.correctAnswer) {
            // add to the number of correct answers
            numCorrect++;
            
            // move the balon image
            if (currentSlide - 2 === questionNumber) {
              balon.style.transform = `scale(1.${questionNumber + 1})`
              
              if (currentSlide === slides.length - 1) {
                balon.style.bottom = bottom + 1500 + 'px'
              }
              else {
                const height = Number(document.getElementById('game').clientHeight) / 1.5
                balon.style.bottom = bottom + (height / slides.length) + 'px'
              }
            }
          }

          // if answer is wrong or blank
          else {
            if (bottom > 20) {
              const height = Number(document.getElementById('game').clientHeight) / 1.5
              balon.style.bottom = bottom - (height / slides.length) + 'px'
            }
            // document.querySelector(`#check-${questionNumber}:checked ~ .checkmark`).style.backgroundColor = 'red'
          }
        }
      });
  
      // show number of correct answers out of total
      resultsContainer.innerHTML = `${numCorrect} out of ${myQuestions.length}`;
    }
  
    function showSlide(n) {
      slides[currentSlide].classList.remove('active-slide');
      slides[n].classList.add('active-slide');
      currentSlide = n;
      restartButton.style.display = 'none';
      submitButton.style.display = 'none';
      document.getElementById('results').style.display = 'none'
      if (currentSlide === 0) {
        previousButton.style.display = 'none';
        nextButton.style.display = 'none';
      }
      if (currentSlide === 1) {
        startButton.style.display = 'none';
        nextButton.style.display = 'inline-block';
        previousButton.style.display = 'none';
      }
      if (currentSlide > 1 && currentSlide < slides.length - 2) {
        previousButton.style.display = 'inline-block';
      }
      if (currentSlide === slides.length - 1) {
        submitButton.style.display = 'none';
        nextButton.style.display = 'none';
        previousButton.style.display = 'none'
        restartButton.style.display = 'inline-block';
        // play audio
        audio2.play();
        document.getElementById('results').style.display = 'block'
      }
      if (currentSlide === slides.length - 2) {
        startButton.style.display = 'none';
        nextButton.style.display = 'none';
        previousButton.style.display = 'none';
        submitButton.style.display = 'inline-block';
      }
    }
  
    function showNextSlide() {
      showSlide(currentSlide + 1);
      // play audio
      audio1.play();
      if (currentSlide > 1) showResults()
    }
  
    function showPreviousSlide() {
      numCorrect--
      const bottom = Number(balon.style.bottom.split('p')[0])
      const height = Number(document.getElementById('game').clientHeight) / 1.5
      balon.style.bottom = bottom - (height / slides.length) + 'px'
      showSlide(currentSlide - 1);
      // play audio
      // audio1.play();
      // showResults()
    }
  
    // Variables
    const quizContainer = document.getElementById('quiz');
    const resultsContainer = document.getElementById('results')
    const submitButton = document.getElementById('submit');
    const startButton = document.getElementById('start');
    const restartButton = document.getElementById('restart');
    const balon = document.getElementById('balon');
    const audio1 = new Audio("../sounds/click.wav");
    const audio2 = new Audio("../sounds/done.wav");
    let numCorrect = 0;

    const myQuestions = [
      {
        question: "../img/notes/a.jpg",
        answers: {
          a: "Note is : C",
          b: "Note is : A",
          c: "Note is : B"
        },
        correctAnswer: "b"
      },
      {
        question: "../img/notes/b.jpg",
        answers: {
          a: "Note is : C",
          b: "Note is : D",
          c: "Note is : B"
        },
        correctAnswer: "c"
      },
      {
        question: "../img/notes/c.jpg",
        answers: {
          a: "Note is : C",
          b: "Note is : F",
          c: "Note is : A"
        },
        correctAnswer: "a"
      },
      {
        question: "../img/notes/d.jpg",
        answers: {
          a: "Note is : G",
          b: "Note is : D",
          c: "Note is : F"
        },
        correctAnswer: "b"
      },
      {
        question: "../img/notes/e.jpg",
        answers: {
          a: "Note is : C",
          b: "Note is : D",
          c: "Note is : E"
        },
        correctAnswer: "c"
      },
      {
        question: "../img/notes/f.jpg",
        answers: {
          a: "Note is : G",
          b: "Note is : B",
          c: "Note is : F"
        },
        correctAnswer: "c"
      },
      {
        question: "../img/notes/g.jpg",
        answers: {
          a: "Note is : C",
          b: "Note is : G",
          c: "Note is : F"
        },
        correctAnswer: "b"
      }
    ];
    myQuestions.sort(() => (Math.random() > .5) ? 1 : -1);
  
    // Kick things off
    buildQuiz();
  
    // Pagination
    const previousButton = document.getElementById("previous");
    const nextButton = document.getElementById("next");
    const slides = document.querySelectorAll(".slide");
    let currentSlide = 0;
     
    // set balon image position
    balon.style.bottom = '20px'

    // Show the first slide
    showSlide(currentSlide);

    // Sow Initial result
    // showResults()
  
    // Event listeners
    submitButton.addEventListener('click', () => {
        showSlide(currentSlide + 1);
        showResults()
        // document.getElementById('quiz').style.display = 'none';
        // document.getElementById('thanks').style.display = 'block';
        previousButton.style.display = 'none';
        submitButton.style.display = 'none';
        nextButton.style.display = 'none';
        startButton.style.display = 'none';
        restartButton.style.display = 'inline-block';
    });
    startButton.addEventListener("click", showNextSlide)
    previousButton.addEventListener("click", showPreviousSlide);
    nextButton.addEventListener("click", showNextSlide);
    restartButton.addEventListener("click", () => {
      numCorrect = 0
      startButton.style.display = 'inline-block';
      radios = document.getElementsByClassName('answer-radio')
      for (let index = 0; index < radios.length; index++) {
          if (radios[index].checked) radios[index].checked = false
      }
      resultsContainer.innerHTML = `0 out of ${myQuestions.length}`
      myQuestions.sort(() => (Math.random() > .5) ? 1 : -1);
      showSlide(0)
      balon.style.transform = `scale(1)`
      balon.style.bottom = '20px'
    });
  })();
  